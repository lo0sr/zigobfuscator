# ZigObfuscator

## Getting started
```
git clone https://gitlab.com/lo0sr/zigobfuscator.git
zig build -Doptimize=ReleaseSmall run
```
## Description
Compile time string obfuscation library written in Zig.

## Usage
See src/main.zig for example usage.

## TODO
Add compile-time PRNG (Pseudo Random Number Generator), with a counter, so that we can better randomize keys used.

## License
MIT
