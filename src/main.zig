const std = @import("std");
const obfuscate = @import("obfuscator.zig").obfuscate;

pub fn main() void {
    std.debug.print("{s}: {s}\n", .{ obfuscate("Decrypted"), obfuscate("Hello 1") });
    std.debug.print("{s}: {s}\n", .{ obfuscate("Decrypted"), obfuscate("Hello 12") });
    std.debug.print("{s}: {s}\n", .{ obfuscate("Decrypted"), obfuscate("Hello 123") });
    std.debug.print("{s}: {s}\n", .{ obfuscate("Decrypted"), obfuscate("Hello 1234") });
    std.debug.print("{s}: {s}\n", .{ obfuscate("Decrypted"), obfuscate("Hello 12345") });
    std.debug.print("{s}: {s}\n", .{ obfuscate("Decrypted"), obfuscate("Hello 123456") });
    std.debug.print("{s}: {s}\n", .{ obfuscate("Decrypted"), obfuscate("Hello 1234567") });
    std.debug.print("{s}: {s}\n", .{ obfuscate("Decrypted"), obfuscate("Hello 12345678") });
    std.debug.print("{s}: {s}\n", .{ obfuscate("Decrypted"), obfuscate("Hello 123456789") });
}
