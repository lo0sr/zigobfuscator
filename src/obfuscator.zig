const std = @import("std");
const config = @import("config");

fn truncateU64toU8(value: u64) u8 {
    return @as(u8, @intCast(value & 0xFF)); // Taking only the least significant 8 bits
}

fn xor_string_encrypt(comptime input: []const u8, comptime key: u64) [input.len]u8 {
    var output: [input.len]u8 = undefined;
    inline for (input, 0..) |char, index| {
        output[index] = char ^ comptime truncateU64toU8(key);
    }
    return output;
}

fn xor_string_decrypt(output: []volatile u8, input: []volatile u8, key: u64) void {
    for (input, 0..) |char, index| {
        output[index] = char ^ truncateU64toU8(key);
    }
}

fn atbash_cipher_encrypt(comptime input: []const u8, comptime key: u64) [input.len]u8 {
    var output: [input.len]u8 = undefined;
    _ = key;

    inline for (input, 0..) |char, index| {
        const reverseChar = 127 - char; // Reverse in the ASCII range
        output[index] = reverseChar;
    }

    return output;
}

fn atbash_cipher_decrypt(output: []volatile u8, input: []volatile u8, key: u64) void {
    _ = key;
    for (input, 0..) |char, index| {
        output[index] = 127 - char; // Reverse operation for decryption
    }
}

fn shift_cipher_encrypt(comptime input: []const u8, comptime key: u64) [input.len]u8 {
    var output: [input.len]u8 = undefined;

    inline for (input, 0..) |char, index| {
        const modKey = key % 128; // Modulo by the ASCII range
        output[index] = (char + modKey) % 128;
    }

    return output;
}

fn shift_cipher_decrypt(output: []volatile u8, input: []volatile u8, key: u64) void {
    for (input, 0..) |char, index| {
        const modKey: u8 = truncateU64toU8(key % 128);
        output[index] = (128 + char - modKey) % 128;
    }
}

fn autokey_cipher_encrypt(comptime input: []const u8, comptime key: u64) [input.len]u8 {
    var output: [input.len]u8 = undefined;
    var keystream: [input.len]u8 = undefined;

    // Initialize keystream with the key followed by the input
    const modKey = truncateU64toU8(key % 128);
    keystream[0] = modKey;
    inline for (input[0 .. input.len - 1], 1..) |char, index| {
        keystream[index] = char;
    }

    // Encryption
    inline for (input, 0..) |char, index| {
        output[index] = (char + keystream[index]) % 128;
    }

    return output;
}

fn autokey_cipher_decrypt(output: []volatile u8, input: []volatile u8, key: u64) void {
    var keystream = std.heap.page_allocator.alloc(u8, input.len) catch {
        return;
    };

    const modKey = truncateU64toU8(key % 128);
    keystream[0] = modKey;

    for (input, 0..) |char, index| {
        output[index] = (128 + char - keystream[index]) % 128;

        if (index + 1 < input.len) {
            keystream[index + 1] = output[index];
        }
    }

    std.heap.page_allocator.free(keystream);
}

const ObfuscatedString = struct {
    encryptedValue: []const u8,
    key: u64,
    encryptFn: fn (comptime []const u8, comptime u64) []const u8,
    decryptFn: fn ([]volatile u8, []volatile u8, u64) void,

    pub fn init(comptime len: u64) ObfuscatedString {
        return switch ((config.base_prn + len) % 4) {
            0 => .{ .encryptedValue = undefined, .key = config.base_prn + len, .encryptFn = xor_string_encrypt, .decryptFn = xor_string_decrypt },
            1 => .{ .encryptedValue = undefined, .key = config.base_prn + len, .encryptFn = shift_cipher_encrypt, .decryptFn = shift_cipher_decrypt },
            2 => .{ .encryptedValue = undefined, .key = config.base_prn + len, .encryptFn = atbash_cipher_encrypt, .decryptFn = atbash_cipher_decrypt },
            3 => .{ .encryptedValue = undefined, .key = config.base_prn + len, .encryptFn = autokey_cipher_encrypt, .decryptFn = autokey_cipher_decrypt },
            else => .{ .encryptedValue = undefined, .key = config.base_prn, .encryptFn = xor_string_encrypt, .decryptFn = xor_string_decrypt },
        };
    }

    pub fn encrypt(comptime self: ObfuscatedString, comptime string: []const u8) ObfuscatedString {
        return .{ .encryptedValue = comptime &self.encryptFn(string, self.key), .key = self.key, .encryptFn = self.encryptFn, .decryptFn = self.decryptFn };
    }

    pub fn decrypt(comptime self: ObfuscatedString, output: []volatile u8) void {
        self.decryptFn(output, @constCast(self.encryptedValue), self.key);
    }
};

pub fn obfuscate(comptime string: []const u8) [string.len]u8 {
    var output: [string.len]u8 = undefined;

    const encrypted_string = comptime ObfuscatedString.init(string.len).encrypt(string);
    encrypted_string.decrypt(output[0..]);

    return output;
}
